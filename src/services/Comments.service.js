
import { CONSTANTS } from '../constants/Constants'

// export async function getComments(storyID) {
//   const story = await mapToJson(storyID);
//   story.comments = [];
//
//   if("kids" in story){
//     for (const id of story.kids){
//       const child = await getComments(id)
//       story.comments.push(child)
//     }
//   }
//   return story
// }

export function mapToJson(storyID){
  return fetch(`${ CONSTANTS.BASE_URL }item/${storyID}.json`)
    .then(response => response.json())
    .catch(error => {
      console.error('Error:', error);
    });
}
