
01 - Setup project and added basic files
02 - Added basic Navbar component
03 - Added basic TopStories (with displayed title)
04 - Added basic TopStory
05 - added link to TopStory and remove item from Navbar
06 - added Comments component and router
07 - added Comment component, service HackerAPI and basic displayed comments
08 - added markup and expand/collapse for comments
09 - separated services
10 - added pagination and changed TopStories.service
11 - added functionality - open story in new tab
10 - added arrows expand/collapse to comments
11 - added Constants


